﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HROneman
{
    internal class DoorSwitch
    {
        public bool IsSwitchOn { get; set; } = false;
        public bool IsReopening { get; set; } = false;
        public bool IsOpened { get; set; } = false;
    }
}
