﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HROneman
{
    internal enum ConductorMode
    {
        Twoman = 0,
        TwomanAtNextStation = 1,
        Oneman = 2,
        OnemanAtNextStation = 3,
    }
}
