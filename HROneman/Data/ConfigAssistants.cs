﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HROneman.Data
{
    /// <summary>
    /// 補助表示設定クラス
    /// </summary>
    public class ConfigAssistants
    {
        /// <summary>
        /// 補助表示設定リスト
        /// </summary>
        public List<Assistant> Settings { get; set; }


        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ConfigAssistants()
        {
            this.Settings = new List<Assistant>();
        }


        /// <summary>
        /// 補助表示設定スキーマ
        /// </summary>

        public class Assistant
        {
            /// <summary>
            /// 項目名
            /// </summary>
            public string Name { get; set; }

            /// <summary>
            /// フォントサイズ
            /// </summary>
            public int Size { get; set; }

            /// <summary>
            /// 座標基準
            /// </summary>
            public int Anchor { get; set; }

            /// <summary>
            /// 座標基準からのX座標
            /// </summary>
            public int X { get; set; }

            /// <summary>
            /// 座標基準からのY座標
            /// </summary>
            public int Y { get; set; }
        }
    }
}
