﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Unicode;
using Mackoy.Bvets;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace HROneman.Data
{

    public class Config
    {
        /// <summary>
        /// ワンマンプラグイン設定
        /// </summary>
        public ConfigSchema Schema;
        /// <summary>
        /// 補助表示設定
        /// </summary>
        public ConfigAssistants AssistantSettings;


        /// <summary>
        /// 読み込み
        /// </summary>
        /// <param name="throwExceptionIfNotExists"></param>
        public void Load(bool throwExceptionIfNotExists)
        {
            Load(EngineBase.ModulePathWithFileName.Replace(".dll", ".json"), throwExceptionIfNotExists);
        }
        /// <summary>
        /// 指定したパスを読み込み
        /// </summary>
        /// <param name="schemaPath">ワンマンプラグイン設定のパス</param>
        /// <param name="throwExceptionIfNotExists"></param>
        public void Load(string schemaPath, bool throwExceptionIfNotExists)
        {
            Schema = Deserialize(schemaPath, throwExceptionIfNotExists);
            AssistantSettings = DeserializeAssistants(throwExceptionIfNotExists);
        }

        /// <summary>
        /// 保存
        /// </summary>
        public void Save()
        {
            Save(EngineBase.ModulePathWithFileName.Replace(".dll", ".json"));
        }
        /// <summary>
        /// 指定したパスに保存
        /// </summary>
        /// <param name="schemaPath">ワンマンプラグイン設定の保存先</param>
        public void Save(string schemaPath)
        {
            Serialize(schemaPath);
            SerializeAssistants();
        }
        /// <summary>
        /// 補助表示設定を保存
        /// </summary>
        public void SaveAssistant()
        {
            SerializeAssistants();
        }


        private void Serialize(string JsonPath)
        {
            var options = new JsonSerializerOptions
            {
                Encoder = JavaScriptEncoder.Create(UnicodeRanges.All),
                WriteIndented = true
            };
            //ConfigSchema cs = new ConfigSchema();
            //cs.Route = Route;
            //cs.Vehicle = Vehicle;
            string jsonString = JsonSerializer.Serialize(Schema, options);
            File.WriteAllText(JsonPath, jsonString);
        }

        private ConfigSchema Deserialize(string JsonPath, bool throwExceptionIfNotExists)
        {
            EngineBase.Log.Write("[Config.Deserialize] " + JsonPath);
            if (!File.Exists(JsonPath) && !throwExceptionIfNotExists) return new ConfigSchema();

            ConfigSchema cs = new ConfigSchema();
            try
            {
                string jsonText = File.ReadAllText(JsonPath);
                cs = JsonSerializer.Deserialize<ConfigSchema>(jsonText);
                EngineBase.Log.Write("[Config.Deserialize] (ok)");
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[Config.Deserialize] (error) " + ex.Message);
                return new ConfigSchema();
            }
            return cs;
        }

        //public static Config Deserialize(bool throwExceptionIfNotExists)
        //    => Deserialize(Path.GetFileNameWithoutExtension(EngineBase.ModulePathWithFileName), throwExceptionIfNotExists);


        private void SerializeAssistants()
        {
            try
            {
                string dirPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData) + "\\hamatetsu\\";
                if (System.IO.Directory.Exists(dirPath) == false)
                {
                    System.IO.Directory.CreateDirectory(dirPath);
                }

                var options = new JsonSerializerOptions
                {
                    Encoder = JavaScriptEncoder.Create(UnicodeRanges.All),
                    WriteIndented = true
                };
                string jsonString = JsonSerializer.Serialize(AssistantSettings, options);
                File.WriteAllText(dirPath + "AssistantSettings.json", jsonString);
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[Config.SerializeAssistants] (error) " + ex.Message);
            }
        }

        private ConfigAssistants DeserializeAssistants(bool throwExceptionIfNotExists)
        {
            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData) + "\\hamatetsu\\AssistantSettings.json";
            EngineBase.Log.Write("[Config.DeserializeAssistants] " + path);
            if (!File.Exists(path) && !throwExceptionIfNotExists) return new ConfigAssistants();

            ConfigAssistants config = new ConfigAssistants();
            try
            {
                string jsonText = File.ReadAllText(path);
                config = JsonSerializer.Deserialize<ConfigAssistants>(jsonText);
                EngineBase.Log.Write("[Config.DeserializeAssistants] (ok)");
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[Config.DeserializeAssistants] (error) " + ex.Message);
            }
            return config;
        }

        #region 補助表示関係
        /// <summary>
        /// 補助表示設定リストに指定名称が存在するか
        /// </summary>
        /// <param name="name">名称</param>
        /// <returns></returns>
        public bool ExistsAssistant(string name)
        {
            try
            {
                foreach (ConfigAssistants.Assistant AS in this.AssistantSettings.Settings)
                {
                    if (AS.Name == name) return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(name + "\n" + ex.Message, "ExistsAssistant");
                return false;
            }
        }
        /// <summary>
        /// 補助表示設定リストから指定名称の設定を取得(見つからなかった場合は新規追加)
        /// </summary>
        /// <param name="name">名称</param>
        /// <param name="size">フォントサイズ</param>
        /// <returns></returns>
        public ConfigAssistants.Assistant GetAssistant(string name, int size = 18)
        {
            ConfigAssistants.Assistant retval = null;
            try
            {
                foreach (ConfigAssistants.Assistant AS in this.AssistantSettings.Settings)
                {
                    if (AS.Name == name)
                    {
                        retval = AS; break;
                    }
                }
                if (retval == null)
                {
                    // 新規追加
                    retval = new ConfigAssistants.Assistant();
                    retval.Name = name;
                    retval.Size = size;
                    retval.Anchor = (int)(AnchorStyles.Top | AnchorStyles.Left);
                    retval.X = 0;
                    retval.Y = 0;
                    this.AssistantSettings.Settings.Add(retval);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(name + "\n" + ex.Message, "GetAssistant");
            }
            return retval;
        }
        #endregion
    }
}
