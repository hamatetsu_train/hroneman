﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HROneman.Data
{
    public class ConfigSchema
    {
        /// <summary>
        /// 路線関係の設定
        /// </summary>
        public Route Route { get; set; }
        /// <summary>
        /// 車両関係の設定
        /// </summary>
        public Vehicle Vehicle { get; set; }
        /// <summary>
        /// 左上の補助表示を表示するか？
        /// </summary>
        public bool ShowDebugLabel { get; set; }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ConfigSchema() 
        {
            Route = new Route();
            Vehicle = new Vehicle();
#if DEBUG
            ShowDebugLabel = true;
#else
            ShowDebugLabel = false;
#endif
        }
    }


    #region キーアサイン
    /// <summary>
    /// キー入力種類
    /// </summary>
    public enum KeyKind_ENUM
    {
        キーボード = 0,
        ATSキー = 1,
        外部入力 = 2
    }

    /// <summary>
    /// キー設定
    /// </summary>
    public class KeyInput
    {
        /// <summary>
        /// 種類
        /// </summary>
        public KeyKind_ENUM Kind { get; set; }
        /// <summary>
        /// キーコード
        /// </summary>
        public int Code { get; set; }
        /// <summary>
        /// 備考
        /// </summary>
        public string Memo { get; set; }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KeyInput()
        {
            new KeyInput(KeyKind_ENUM.キーボード, 0);
        }
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="kind"></param>
        /// <param name="code"></param>
        public KeyInput(KeyKind_ENUM kind, int code)
        {
            Kind = kind;
            Code = code;
            Memo = "";
        }

        /// <summary>
        /// 同値かどうか
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equal(KeyInput other)
        {
            return (this.Kind == other.Kind)
                && (this.Code == other.Code);
        }
    }

    /// <summary>
    /// キーアサイン
    /// </summary>
    public class KeyAssignSet
    {
        /// <summary>
        /// 左ドア開SW
        /// </summary>
        public KeyInput LeftOpen { get; set; }
        /// <summary>
        /// 左ドア締SW
        /// </summary>
        public KeyInput LeftClose { get; set; }
        /// <summary>
        /// 左ドア再開扉SW
        /// </summary>
        public KeyInput LeftReopen { get; set; }
        /// <summary>
        /// 右ドア開SW
        /// </summary>
        public KeyInput RightOpen { get; set; }
        /// <summary>
        /// 右ドア締SW
        /// </summary>
        public KeyInput RightClose { get; set; }
        /// <summary>
        /// 右ドア再開扉SW
        /// </summary>
        public KeyInput RightReopen { get; set; }

        //public KeyInput RequestFixStopPosition { get; set; }
        //public KeyInput ConductorValve { get; set; }

        /// <summary>
        /// ワンマンNFB
        /// </summary>
        //public KeyInput OnemanNFB { get; set; }
        /// <summary>
        /// 半自動・自動SW
        /// </summary>
        public KeyInput SemiautoSW { get; set; }
        /// <summary>
        /// 全車・自車SW
        /// </summary>
        public KeyInput OneDoorSW { get; set; }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KeyAssignSet()
        {
            LeftOpen = new KeyInput(KeyKind_ENUM.キーボード, (int)Keys.E);
            LeftClose = new KeyInput(KeyKind_ENUM.キーボード, (int)Keys.R);
            LeftReopen = new KeyInput(KeyKind_ENUM.キーボード, (int)Keys.T);

            RightOpen = new KeyInput(KeyKind_ENUM.キーボード, (int)Keys.O);
            RightClose = new KeyInput(KeyKind_ENUM.キーボード, (int)Keys.I);
            RightReopen = new KeyInput(KeyKind_ENUM.キーボード, (int)Keys.U);

            //RequestFixStopPosition = new KeyInput();
            //ConductorValve = new KeyInput();

            //OnemanNFB = new KeyInput();
            SemiautoSW = new KeyInput();
            OneDoorSW = new KeyInput();
        }
    }
    #endregion

    #region パネル
    /// <summary>
    /// パネル設定
    /// </summary>
    public class AtsPanelValueSet
    {
        /// <summary>
        /// モード
        /// </summary>
        public int Mode { get; set; }
        /// <summary>
        /// NFB
        /// </summary>
        public int NFB { get; set; }
        /// <summary>
        /// 自動・半自動(T:半自動)
        /// </summary>
        public int Semiauto { get; set; }
        /// <summary>
        /// 自車・全車(T:自車)
        /// </summary>
        public int OneDoor { get; set; }


        /// <summary>
        /// コンストラクタ
        /// </summary>
        public AtsPanelValueSet()
        {
            Mode = -1;
            NFB = -1;
            Semiauto = -1;
            OneDoor = -1;
        }
    }
    #endregion

    #region サウンド
    /// <summary>
    /// サウンド設定
    /// </summary>
    public class AtsSoundSet
    {
        /// <summary>
        /// ドアスイッチ開操作
        /// </summary>
        public int DoorSwitchOn { get; set; }
        /// <summary>
        /// ドアスイッチ閉
        /// </summary>
        public int DoorSwitchOff { get; set; }
        /// <summary>
        /// 半自動でドア開した場合
        /// </summary>
        public int DoorSemiautoOpen { get; set; }
        /// <summary>
        /// 半自動でドア締した場合
        /// </summary>
        public int DoorSemiautoClose { get; set; }


        /// <summary>
        /// コンストラクタ
        /// </summary>
        public AtsSoundSet() 
        {
            DoorSwitchOn = -1;
            DoorSwitchOff = -1;
            DoorSemiautoOpen = -1;
            DoorSemiautoClose = -1;
        }
    }
    #endregion

    /// <summary>
    /// 車両設定
    /// </summary>
    public class Vehicle
    {
        /// <summary>
        /// 初期状態 true:ワンマン
        /// </summary>
        public bool IsEnabledByDefault { get; set; }
        /// <summary>
        /// パネル
        /// </summary>
        public AtsPanelValueSet AtsPanelValues { get; set; }
        /// <summary>
        /// サウンド
        /// </summary>
        public AtsSoundSet AtsSounds { get; set; }
        /// <summary>
        /// キーアサイン
        /// </summary>
        public KeyAssignSet KeyAssign { get; set; }


        /// <summary>
        /// コンストラクタ
        /// </summary>
        public Vehicle()
        {
            IsEnabledByDefault = true;
            AtsPanelValues = new AtsPanelValueSet();
            AtsSounds = new AtsSoundSet();
            KeyAssign = new KeyAssignSet();
        }
    }

    /// <summary>
    /// 地上子
    /// </summary>
    public class BeaconSet
    {
        /// <summary>
        /// モード変更
        /// </summary>
        public int ChangeEnabledBeacon { get; set; }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public BeaconSet()
        {
            ChangeEnabledBeacon = -1;
        }
    }

    /// <summary>
    /// 路線設定
    /// </summary>
    public class Route
    {
        /// <summary>
        /// 地上子設定
        /// </summary>
        public BeaconSet Beacons { get; set; }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public Route()
        {
            Beacons = new BeaconSet();
        }
    }

}
