﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BveTypes.ClassWrappers;

using BveEx.Extensions;
using BveEx.Extensions.ConductorPatch;
using Mackoy.Bvets;
using System.Xml.Linq;

namespace HROneman
{
    internal class ManualConductor : ConductorBase
    {
        private static readonly DoorSide[] DoorSides = (DoorSide[])Enum.GetValues(typeof(DoorSide));
        private static readonly Random Random = new Random();

        private readonly StationListEx StationListEx;

        private readonly Dictionary<int, DoorSwitch> DoorSwitches = new Dictionary<int, DoorSwitch>()
        {
            { (int)DoorSide.Left, new DoorSwitch() },
            { (int)DoorSide.Right, new DoorSwitch() },
        };

        private bool HasStopPositionChecked = false;
        private TimeSpan MinDepartureSoundPlayTime = TimeSpan.Zero;

        private bool IsMoving => 5 / 3.6 <= Math.Abs(Original.Location.Speed);

        public bool IsLeftReopening
        {
            get => DoorSwitches[(int)DoorSide.Left].IsReopening;
            set
            {
                DoorSwitch doorSwitch = DoorSwitches[(int)DoorSide.Left];
                if (doorSwitch.IsReopening && !value && !doorSwitch.IsSwitchOn) FinishReopen(DoorSide.Left);
                doorSwitch.IsReopening = value;
            }
        }

        public bool IsRightReopening
        {
            get => DoorSwitches[(int)DoorSide.Right].IsReopening;
            set
            {
                DoorSwitch doorSwitch = DoorSwitches[(int)DoorSide.Right];
                if (doorSwitch.IsReopening && !value && !doorSwitch.IsSwitchOn) FinishReopen(DoorSide.Right);
                doorSwitch.IsReopening = value;
            }
        }

        private void FinishReopen(DoorSide doorSide)
        {
            (_, Station nextStation) = StationListEx.GetStation(1);
            SideDoorSet sideDoors = Original.Doors.GetSide(doorSide);

            foreach (CarDoor door in sideDoors.CarDoors)
            {
                if (door.IsOpen) door.Close((int)(nextStation.StuckInDoorMilliseconds * Random.NextDouble() * Random.NextDouble()));
            }
        }

        /// <summary>
        /// すべてのドアが閉まっているか？
        /// </summary>
        public bool IsDoorClosed
        {
            get { if (!Original.Doors.GetSide(DoorSide.Left).IsOpen && !Original.Doors.GetSide(DoorSide.Right).IsOpen) return true; else return false; }
        }

        private bool pSemiautoSW = false;
        /// <summary>
        /// 自動・半自動スイッチ (T:半自動)
        /// </summary>
        public bool SemiautoSW
        {
            get { return pSemiautoSW; }
        }

        private bool pOneDoorSW = true;
        /// <summary>
        /// 自車・全車スイッチ (T:全車 <= [全車]ランプを光らせたかったからこうなってる)
        /// </summary>
        public bool OneDoorSW
        {
            get { return pOneDoorSW; }
        }

        protected override event EventHandler FixStopPositionRequested;
        protected override event EventHandler StopPositionChecked;
        protected override event EventHandler DoorOpening;
        protected override event EventHandler DepartureSoundPlaying;
        protected override event EventHandler DoorClosing;
        protected override event EventHandler DoorClosed;

        public ManualConductor(Conductor original) : base(original)
        {
            StationListEx = new StationListEx(Original.Stations, () => Original.Location.Location);
        }

        public void Sync()
        {
            EngineBase.Log.Write("[ManualConductor.Sync]");
            OpenOrClose(DoorSide.Left);
            OpenOrClose(DoorSide.Right);


            void OpenOrClose(DoorSide side)
            {
                SideDoorSet doors = Original.Doors.GetSide(side);
                if (doors.IsOpen) OpenDoors(side); else CloseDoors(side);
            }
        }

        public (int nextStationIndex, Station nextStation) Desync()
        {
            EngineBase.Log.Write("[ManualConductor.Desync]");
            IsLeftReopening = false;
            IsRightReopening = false;

            CloseDoors(DoorSide.Left);
            CloseDoors(DoorSide.Right);

            (int nextStationIndex, Station nextStation) = StationListEx.GetStation(1);
            
            SideDoorSet leftDoors = Original.Doors.GetSide(DoorSide.Left);
            SideDoorSet rightDoors = Original.Doors.GetSide(DoorSide.Right);

            if (nextStation is null || !(Math.Abs(Original.Location.Speed) < 0.01f && StationListEx.IsAtValidPosition(nextStationIndex)) || !nextStation.DoorSide.HasValue)
            {
                leftDoors.CloseDoors(0);
                rightDoors.CloseDoors(0);
            }
            else
            {
                switch (nextStation.DoorSide.Value)
                {
                    case DoorSide.Left:
                        leftDoors.OpenDoors();
                        rightDoors.CloseDoors(0);
                        break;

                    case DoorSide.Right:
                        leftDoors.CloseDoors(0);
                        rightDoors.OpenDoors();
                        break;

                    default:
                        leftDoors.CloseDoors(0);
                        rightDoors.CloseDoors(0);
                        break;
                }
            }

            return (nextStationIndex, nextStation);
        }

        protected override MethodOverrideMode OnJumped(int stationIndex, bool isDoorClosed)
        {
            HasStopPositionChecked = false;

            Original.Stations.GoToByIndex(stationIndex - 1);
            Original.Doors.SetState(DoorState.Close, DoorState.Close);

            CloseDoors(DoorSide.Left);
            CloseDoors(DoorSide.Right);
            DoorSwitches[(int)DoorSide.Left].IsOpened = false;
            DoorSwitches[(int)DoorSide.Right].IsOpened = false;

            Station currentStation = Original.Stations.Count <= stationIndex ? null : Original.Stations[stationIndex] as Station;
            //int doorSide = currentStation is null || currentStation.Pass || isDoorClosed ? 0 : (int)currentStation.DoorSide;
            //if (doorSide == 0) Original.Stations.GoToByIndex(stationIndex);
            int doorSide = -1;
            if (currentStation is null || currentStation.Pass || isDoorClosed || !currentStation.DoorSide.HasValue)
            {
                doorSide = -1;
            }
            else
            {
                doorSide = (int)currentStation.DoorSide.Value;
            }
            if (doorSide == -1) Original.Stations.GoToByIndex(stationIndex);

            Sync();
            EngineBase.Log.Write("[ManualConductor.OnJumped] StaIndex=" + stationIndex.ToString() + " DoorSide=" + doorSide.ToString());

            return MethodOverrideMode.SkipOriginal;
        }

        protected override MethodOverrideMode OnDoorStateChanged()
        {
            if (Original.Doors.AreAllClosed && HasStopPositionChecked)
            {
                HasStopPositionChecked = false;
                Original.Stations.GoToByIndex(Original.Stations.CurrentIndex + 1);
                DoorClosed(this, EventArgs.Empty);
                EngineBase.Log.Write("[ManualConductor.OnDoorStateChanged] 0");
            }
            else
            {
                EngineBase.Log.Write("[ManualConductor.OnDoorStateChanged] 1");
            }

            return MethodOverrideMode.SkipOriginal;
        }

        protected override MethodOverrideMode OnTick()
        {
            (int nextStationIndex, Station nextStation) = StationListEx.GetStation(1); // 次駅を取得
            if (!(nextStation is null))
            {
                if (nextStation.Pass || nextStation.DoorSide.HasValue == false) // 次駅が通過or運転停車
                {
                    double location = Original.Location.Location;
                    if ((Math.Abs(Original.Location.Speed) < 0.01f && location >= nextStation.MinStopPosition) || location >= nextStation.MaxStopPosition)
                    {
                        Original.Stations.GoToByIndex(Original.Stations.CurrentIndex + 1);
                    }
                }
                else
                {
                    TimeSpan now = Original.TimeManager.Time;
                    if (MinDepartureSoundPlayTime != TimeSpan.Zero
                        && HasStopPositionChecked
                        && MinDepartureSoundPlayTime <= now
                        && nextStation.DepartureTime - nextStation.StoppageTime <= now
                        && 0 < Original.SectionManager.ForwardSectionSpeedLimit)
                    {
                        MinDepartureSoundPlayTime = TimeSpan.Zero;

                        StationListEx.GetStation(1).Station?.DepartureSound?.Play(1, 1, 0);
                        DepartureSoundPlaying(this, EventArgs.Empty);
                    }
                }
            }

            foreach (DoorSide doorSide in DoorSides)
            {
                DoorSwitch doorSwitch = DoorSwitches[(int)doorSide];
                SideDoorSet sideDoors = Original.Doors.GetSide(doorSide);

                // ドアスイッチON && 5km/h以下
                if (doorSwitch.IsSwitchOn && !IsMoving)
                {
                    // ドアが開いていない
                    if (!doorSwitch.IsOpened)
                    {
                        // ドアを開ける
                        if (!(nextStation is null) && StationListEx.IsNearestStation(nextStationIndex) && nextStation.DoorSide == doorSide) // ToDoorSideNumber(doorSide))
                        {
                            OpenDoorsAt(nextStation, !HasStopPositionChecked);
                            EngineBase.Log.Write("[ManualConductor.OnTick] 0");
                        }
                        else
                        {
                            (int stationIndex, Station station) = StationListEx.GetStation(0);

                            if (!(station is null) && StationListEx.IsNearestStation(stationIndex) && station.DoorSide == doorSide) // ToDoorSideNumber(doorSide))
                            {
                                Original.Stations.GoToByIndex(stationIndex - 1);
                                OpenDoorsAt(station, false);
                                EngineBase.Log.Write("[ManualConductor.OnTick] 1");
                            }
                            else
                            {
                                if(this.pOneDoorSW == true)
                                {
                                    // 全車オープン
                                    if(this.pSemiautoSW == false)
                                    {
                                        // 自動
                                        sideDoors.OpenDoors();
                                        EngineBase.Log.Write("[ManualConductor.OnTick] 01 全車 自動 開 " + doorSide.ToString());
                                    }
                                    else
                                    {
                                        // 半自動
                                        //sideDoors.SetState(DoorState.Open);
                                        if(doorSide == DoorSide.Left)
                                        {
                                            Original.Doors.SetState(DoorState.Open, DoorState.Close);
                                            EngineBase.Log.Write("[ManualConductor.OnTick] 02 全車 半自動 開 Left");
                                        }
                                        else
                                        {
                                            Original.Doors.SetState(DoorState.Close, DoorState.Open);
                                            EngineBase.Log.Write("[ManualConductor.OnTick] 02 全車 半自動 開 Right");
                                        }
                                        Original.OnDoorStateChanged(this, EventArgs.Empty);
                                        // 乗車率強制変更
                                        //if (station != null) Original.Passenger.Count = Original.Passenger.Capacity * station.TargetLoadFactor / 100;
                                    }
                                }
                                else
                                {
                                    // 自車オープン
                                    if (this.pSemiautoSW == false)
                                    {
                                        // 自動
                                        sideDoors.CarDoors[0].Open();
                                        EngineBase.Log.Write("[ManualConductor.OnTick] 03 自車 自動 開 " + doorSide.ToString());
                                    }
                                    else
                                    {
                                        // 半自動
                                        //sideDoors.CarDoors[0].SetState(DoorState.Open);
                                        if (doorSide == DoorSide.Left)
                                        {
                                            Original.Doors.SetState(DoorState.Open, DoorState.Close);
                                            EngineBase.Log.Write("[ManualConductor.OnTick] 04 自車 半自動 開 Left");
                                        }
                                        else
                                        {
                                            Original.Doors.SetState(DoorState.Close, DoorState.Open);
                                            EngineBase.Log.Write("[ManualConductor.OnTick] 04 自車 半自動 開 Right");
                                        }
                                        Original.OnDoorStateChanged(this, EventArgs.Empty);
                                        // 乗車率強制変更
                                        //if(station != null) Original.Passenger.Count = Original.Passenger.Capacity * station.TargetLoadFactor / 100;
                                    }
                                }
                            }
                        }

                        doorSwitch.IsOpened = true;
                    }
                }
                // ドアスイッチオフ
                else if(!doorSwitch.IsSwitchOn)
                {
                    // (ドアスイッチオフなのに)ドアが開いている
                    if (doorSwitch.IsOpened)
                    {
                        // ドアを締める
                        if (!(nextStation is null) && StationListEx.IsNearestStation(nextStationIndex) && nextStation.DoorSide == doorSide) // ToDoorSideNumber(doorSide))
                        {
                            if (this.pSemiautoSW == false)
                            {
                                // 自動
                                sideDoors.CloseDoors(nextStation.StuckInDoorMilliseconds);
                                EngineBase.Log.Write("[ManualConductor.OnTick] 11 自動 締 " + doorSide.ToString());
                            }
                            else
                            {
                                // 半自動
                                //sideDoors.SetState(DoorState.Close);
                                Original.Doors.SetState(DoorState.Close, DoorState.Close);
                                //Original.DepartureRequested_Invoke();
                                //Original.OnDoorStateChanged(this, EventArgs.Empty);
                                EngineBase.Log.Write("[ManualConductor.OnTick] 12 半自動 締 " + doorSide.ToString());
                            }
                            DoorClosing(this, EventArgs.Empty);
                        }
                        else
                        {
                            if (this.pSemiautoSW == false)
                            {
                                // 自動
                                sideDoors.CloseDoors(nextStation is null ? 0 : nextStation.StuckInDoorMilliseconds);
                                EngineBase.Log.Write("[ManualConductor.OnTick] 13 自動 締 " + doorSide.ToString());
                            }
                            else
                            {
                                // 半自動
                                //sideDoors.SetState(DoorState.Close);
                                Original.Doors.SetState(DoorState.Close, DoorState.Close);
                                Original.OnDoorStateChanged(this, EventArgs.Empty);
                                //Original.DepartureRequested_Invoke();
                                EngineBase.Log.Write("[ManualConductor.OnTick] 14 半自動 締 " + doorSide.ToString());
                            }
                        }

                        doorSwitch.IsOpened = false;
                    }
                }

                // ドアスイッチオフ && 再開扉オン && 5km/h以下
                if (!doorSwitch.IsSwitchOn && doorSwitch.IsReopening && !IsMoving)
                {
                    if(this.pOneDoorSW == true)
                    {
                        // 全車
                        foreach (CarDoor door in sideDoors.CarDoors)
                        {
                            if (door.IsOpen)
                            {
                                if (this.pSemiautoSW == false)
                                {
                                    // 自動
                                    door.Open();
                                }
                                else
                                {
                                    // 半自動
                                    //door.SetState(DoorState.Open);
                                    if (doorSide == DoorSide.Left)
                                    {
                                        Original.Doors.SetState(DoorState.Open, DoorState.Close);
                                    }
                                    else
                                    {
                                        Original.Doors.SetState(DoorState.Close, DoorState.Open);
                                    }
                                    //Original.OnDoorStateChanged(this, EventArgs.Empty);
                                    //DoorOpening(this, EventArgs.Empty);
                                }
                            }
                        }
                        EngineBase.Log.Write("[ManualConductor.OnTick] 21 全車 再開扉 " + doorSide.ToString());
                    }
                    else
                    {
                        // 自車
                        if (sideDoors.CarDoors[0].IsOpen)
                        {
                            if (this.pSemiautoSW == true)
                            {
                                // 自動
                                sideDoors.CarDoors[0].Open();
                                EngineBase.Log.Write("[ManualConductor.OnTick] 25 自車 自動 再開扉 " + doorSide.ToString());
                            }
                            else
                            {
                                // 半自動
                                sideDoors.CarDoors[0].SetState(DoorState.Open);
                                //DoorOpening(this, EventArgs.Empty);
                                EngineBase.Log.Write("[ManualConductor.OnTick] 26 自車 半自動 再開扉 " + doorSide.ToString());
                            }
                        }
                    }
                }
                else
                {
                    //EngineBase.Log.Write("[OnTick] 29");
                }


                void OpenDoorsAt(Station station, bool isFirstTime)
                {
                    HasStopPositionChecked = true;

                    if (isFirstTime)
                    {
                        StopPositionChecked(this, EventArgs.Empty);

                        MinDepartureSoundPlayTime = Original.TimeManager.Time + TimeSpan.FromSeconds(5);

                        station.ArrivalSound?.Play(1, 1, 0);
                    }

                    if(this.pOneDoorSW == true)
                    {
                        // 全車
                        if (this.pSemiautoSW == false)
                        {
                            // 自動
                            sideDoors.OpenDoors();
                            EngineBase.Log.Write("[ManualConductor.OpenDoorsAt] 01 自動 全車 開 " + doorSide.ToString());
                        }
                        else
                        {
                            // 半自動
                            //sideDoors.SetState(DoorState.Open);
                            if(doorSide == DoorSide.Left)
                            {
                                Original.Doors.SetState(DoorState.Open, DoorState.Close);
                                EngineBase.Log.Write("[ManualConductor.OpenDoorsAt] 02 半自 全車 開 Left");
                            }
                            else
                            {
                                Original.Doors.SetState(DoorState.Close, DoorState.Open);
                                EngineBase.Log.Write("[ManualConductor.OpenDoorsAt] 02 半自 全車 開 Right");
                            }
                            Original.OnDoorStateChanged(this, EventArgs.Empty);
                            // 乗車率強制変更
                            //Original.Passenger.Count = Original.Passenger.Capacity * station.TargetLoadFactor / 100;
                        }
                    }
                    else
                    {
                        // 自車
                        if (this.pSemiautoSW == false)
                        {
                            // 自動
                            sideDoors.CarDoors[0].Open();
                            EngineBase.Log.Write("[ManualConductor.OpenDoorsAt] 03 自動 自車 開 " + doorSide.ToString());
                        }
                        else
                        {
                            // 半自動
                            //sideDoors.CarDoors[0].SetState(DoorState.Open);
                            if (doorSide == DoorSide.Left)
                            {
                                Original.Doors.SetState(DoorState.Open, DoorState.Close);
                                EngineBase.Log.Write("[ManualConductor.OpenDoorsAt] 04L 半自 自車 開 " + doorSide.ToString());
                            }
                            else
                            {
                                Original.Doors.SetState(DoorState.Close, DoorState.Open);
                                EngineBase.Log.Write("[ManualConductor.OpenDoorsAt] 04R 半自 自車 開 " + doorSide.ToString());
                            }
                            if (sideDoors.CarDoors.Count > 1)
                            {
                                for(int i = 1;i < sideDoors.CarDoors.Count;i++)
                                {
                                    sideDoors.CarDoors[i].SetState(DoorState.Close);
                                }
                            }
                            //Original.OnDoorStateChanged(this, EventArgs.Empty);
                        }
                    }
                    DoorOpening(this, EventArgs.Empty);
                }
            }

            return MethodOverrideMode.SkipOriginal;
        }

        public void RequestFixStopPosition()
        {
            FixStopPositionRequested(this, EventArgs.Empty);
        }

        public void OpenDoors(DoorSide doorSide)
        {
            DoorSwitch doorSwitch = DoorSwitches[(int)doorSide];
            doorSwitch.IsSwitchOn = true;
            EngineBase.Log.Write("[ManualConductor.OpenDoors] " + doorSide.ToString());
        }

        public void CloseDoors(DoorSide doorSide)
        {
            DoorSwitch doorSwitch = DoorSwitches[(int)doorSide];
            doorSwitch.IsSwitchOn = false;
            EngineBase.Log.Write("[ManualConductor.CloseDoors] " + doorSide.ToString());
        }

        private int ToDoorSideNumber(DoorSide doorSide) => (int)doorSide * 2 - 1;


        /// <summary>
        /// 自動・半自動スイッチを扱った
        /// </summary>
        public void PushSemiautoSW()
        {
            bool b = true;
            foreach (DoorSide doorSide in DoorSides)
            {
                if(Original.Doors.GetSide(doorSide).IsOpen == true)
                {
                    b = false;
                }
            }
            if(b == true)
            {
                this.pSemiautoSW = !this.pSemiautoSW;
            }
        }

        /// <summary>
        /// 自車・全車スイッチを扱った
        /// </summary>
        public void PushOneDoorSW()
        {
            bool b = true;
            foreach (DoorSide doorSide in DoorSides)
            {
                if (Original.Doors.GetSide(doorSide).IsOpen == true)
                {
                    b = false;
                }
            }
            if (b == true)
            {
                this.pOneDoorSW = !this.pOneDoorSW;
            }
        }
    }
}
