﻿using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;

using BveTypes.ClassWrappers;

using BveEx.PluginHost;
//using BveEx.PluginHost.Input.Native;
//using BveEx.PluginHost.Panels.Native;
//using BveEx.PluginHost.Sound.Native;
using BveEx.PluginHost.Plugins;

using BveEx.Extensions.ConductorPatch;
using HROneman.Data;
using BveEx.Extensions.Native;
//using SlimDX.DirectInput;

namespace HROneman
{
    [Plugin(PluginType.VehiclePlugin)]
    public class PluginMain : AssemblyPluginBase
    {
        private readonly INative Native;

        private readonly HROneman.Data.Config Config;
        private readonly HROneman.Data.ConfigAssistants ConfigAssisntans;
        private readonly ConductorHost ConductorHost = null;

        //private readonly IAtsPanelValue<ConductorMode> ModePanelValue = null;
        //private readonly IAtsPanelValue<Int32> NFBPanelValue = null;
        //private readonly IAtsPanelValue<bool> SemiAutoPanelValue = null;
        //private readonly IAtsPanelValue<bool> OneDoorPanelValue = null;
        //private readonly IAtsSound DoorSwitchOnSound = null;
        //private readonly IAtsSound DoorSwitchOffSound = null;
        //private readonly IAtsSound DoorSemiautoOpenSound = null;
        //private readonly IAtsSound DoorSemiautoCloseSound = null;
        // 前フレームで設定しようとした値
        private int OldDoorSwitchOnSound = -10000;
        private int OldDoorSwitchOffSound = -10000;
        private int OldDoorSemiautoOpenSound = -10000;
        private int OldDoorSemiautoCloseSound = -10000;

        private readonly AssistantText AssistantText;

        private ConductorValve ConductorValve = null;

        //private bool NFB = false;
        private bool IsLeftOpenButtonPushed = false;
        private bool IsLeftCloseButtonPushed = false;
        private bool IsLeftReopenButtonPushed = false;
        private bool IsRightOpenButtonPushed = false;
        private bool IsRightCloseButtonPushed = false;
        private bool IsRightReopenButtonPushed = false;

        public PluginMain(PluginBuilder builder) : base(builder)
        {
            try
            {
                Native = Extensions.GetExtension<INative>();

                EngineBase.Log.Init();
                EngineBase.Log.Write("[PluginMain] " + EngineBase.ModulePathWithFileName);
                EngineBase.Log.Write("[PluginMain] Config Load");
                //Config = Data.Config.Deserialize(EngineBase.ModulePathWithFileName.Replace(".dll", ".json"), false);
                this.Config = new Config();
                this.Config.Load(false);

                EngineBase.Log.Write("[PluginMain] Handler");
                BveHacker.ScenarioCreated += OnScenarioCreated;
                BveHacker.MainFormSource.KeyDown += OnKeyDown;
                Native.AtsKeys.GetKey(BveEx.PluginHost.Input.AtsKeyName.S).Pressed += new EventHandler(KeyS_Press);
                Native.AtsKeys.GetKey(BveEx.PluginHost.Input.AtsKeyName.A1).Pressed += new EventHandler(KeyA1_Press);
                Native.AtsKeys.GetKey(BveEx.PluginHost.Input.AtsKeyName.A2).Pressed += new EventHandler(KeyA2_Press);
                Native.AtsKeys.GetKey(BveEx.PluginHost.Input.AtsKeyName.B1).Pressed += new EventHandler(KeyB1_Press);
                Native.AtsKeys.GetKey(BveEx.PluginHost.Input.AtsKeyName.B2).Pressed += new EventHandler(KeyB2_Press);
                Native.AtsKeys.GetKey(BveEx.PluginHost.Input.AtsKeyName.C1).Pressed += new EventHandler(KeyC1_Press);
                Native.AtsKeys.GetKey(BveEx.PluginHost.Input.AtsKeyName.C2).Pressed += new EventHandler(KeyC2_Press);
                Native.AtsKeys.GetKey(BveEx.PluginHost.Input.AtsKeyName.D).Pressed += new EventHandler(KeyD_Press);
                Native.AtsKeys.GetKey(BveEx.PluginHost.Input.AtsKeyName.E).Pressed += new EventHandler(KeyE_Press);
                Native.AtsKeys.GetKey(BveEx.PluginHost.Input.AtsKeyName.F).Pressed += new EventHandler(KeyF_Press);
                Native.AtsKeys.GetKey(BveEx.PluginHost.Input.AtsKeyName.G).Pressed += new EventHandler(KeyG_Press);
                Native.AtsKeys.GetKey(BveEx.PluginHost.Input.AtsKeyName.H).Pressed += new EventHandler(KeyH_Press);
                Native.AtsKeys.GetKey(BveEx.PluginHost.Input.AtsKeyName.I).Pressed += new EventHandler(KeyI_Press);
                Native.AtsKeys.GetKey(BveEx.PluginHost.Input.AtsKeyName.J).Pressed += new EventHandler(KeyJ_Press);
                Native.AtsKeys.GetKey(BveEx.PluginHost.Input.AtsKeyName.K).Pressed += new EventHandler(KeyK_Press);
                Native.AtsKeys.GetKey(BveEx.PluginHost.Input.AtsKeyName.L).Pressed += new EventHandler(KeyL_Press);
                BveHacker.MainFormSource.KeyUp += OnKeyUp;
                Native.AtsKeys.GetKey(BveEx.PluginHost.Input.AtsKeyName.S).Released += new EventHandler(KeyS_Release);
                Native.AtsKeys.GetKey(BveEx.PluginHost.Input.AtsKeyName.A1).Released += new EventHandler(KeyA1_Release);
                Native.AtsKeys.GetKey(BveEx.PluginHost.Input.AtsKeyName.A2).Released += new EventHandler(KeyA2_Release);
                Native.AtsKeys.GetKey(BveEx.PluginHost.Input.AtsKeyName.B1).Released += new EventHandler(KeyB1_Release);
                Native.AtsKeys.GetKey(BveEx.PluginHost.Input.AtsKeyName.B2).Released += new EventHandler(KeyB2_Release);
                Native.AtsKeys.GetKey(BveEx.PluginHost.Input.AtsKeyName.C1).Released += new EventHandler(KeyC1_Release);
                Native.AtsKeys.GetKey(BveEx.PluginHost.Input.AtsKeyName.C2).Released += new EventHandler(KeyC2_Release);
                Native.AtsKeys.GetKey(BveEx.PluginHost.Input.AtsKeyName.D).Released += new EventHandler(KeyD_Release);
                Native.AtsKeys.GetKey(BveEx.PluginHost.Input.AtsKeyName.E).Released += new EventHandler(KeyE_Release);
                Native.AtsKeys.GetKey(BveEx.PluginHost.Input.AtsKeyName.F).Released += new EventHandler(KeyF_Release);
                Native.AtsKeys.GetKey(BveEx.PluginHost.Input.AtsKeyName.G).Released += new EventHandler(KeyG_Release);
                Native.AtsKeys.GetKey(BveEx.PluginHost.Input.AtsKeyName.H).Released += new EventHandler(KeyH_Release);
                Native.AtsKeys.GetKey(BveEx.PluginHost.Input.AtsKeyName.I).Released += new EventHandler(KeyI_Release);
                Native.AtsKeys.GetKey(BveEx.PluginHost.Input.AtsKeyName.J).Released += new EventHandler(KeyJ_Release);
                Native.AtsKeys.GetKey(BveEx.PluginHost.Input.AtsKeyName.K).Released += new EventHandler(KeyK_Release);
                Native.AtsKeys.GetKey(BveEx.PluginHost.Input.AtsKeyName.L).Released += new EventHandler(KeyL_Release);

                EngineBase.Log.Write("[PluginMain] Beacon");
                BeaconObserver beaconObserver = new BeaconObserver(Native, Config.Schema.Route.Beacons.ChangeEnabledBeacon, Config.Schema.Vehicle.IsEnabledByDefault);

                EngineBase.Log.Write("[PluginMain] Conductor");
                IConductorPatchFactory conductorPatchFactory = Extensions.GetExtension<IConductorPatchFactory>();
                ConductorHost = new ConductorHost(beaconObserver, BveHacker, conductorPatchFactory);

                // パネル設定
                // - ツーマン・ワンマン
                if (-2 < Config.Schema.Vehicle.AtsPanelValues.Mode && Config.Schema.Vehicle.AtsPanelValues.Mode < 1024)
                {
                    //try
                    //{
                    //    ModePanelValue = Native.AtsPanelValues.Register<ConductorMode>(Config.Schema.Vehicle.AtsPanelValues.Mode, x => (int)x);
                    //}
                    //catch (Exception ex)
                    //{
                    //    EngineBase.Log.Write("[PluginMain] PanelValues.Register Mode (error) " + ex.Message);
                    //}
                }
                else
                {
                    EngineBase.Log.Write("Native.AtsPanelValues.Register Mode - OutOfRange:" + Config.Schema.Vehicle.AtsPanelValues.Mode.ToString());
                }
                // - NFB
                if (-2 < Config.Schema.Vehicle.AtsPanelValues.NFB && Config.Schema.Vehicle.AtsPanelValues.NFB < 1024)
                {
                    //try
                    //{
                    //    NFBPanelValue = Native.AtsPanelValues.RegisterInt32(Config.Schema.Vehicle.AtsPanelValues.NFB, 0, AtsEx.PluginHost.Binding.BindingMode.TwoWay);
                    //}
                    //catch (Exception ex)
                    //{
                    //    EngineBase.Log.Write("[PluginMain] PanelValues.Register NFB (error) " + ex.Message);
                    //}
                }
                else
                {
                    EngineBase.Log.Write("Native.AtsPanelValues.Register NFB - OutOfRange:" + Config.Schema.Vehicle.AtsPanelValues.NFB.ToString());
                }
                // - 自動・半自動
                if (-2 < Config.Schema.Vehicle.AtsPanelValues.Semiauto && Config.Schema.Vehicle.AtsPanelValues.Semiauto < 1024)
                {
                    //try
                    //{
                    //    SemiAutoPanelValue = Native.AtsPanelValues.RegisterBoolean(Config.Schema.Vehicle.AtsPanelValues.Semiauto, false, AtsEx.PluginHost.Binding.BindingMode.TwoWay);
                    //}
                    //catch (Exception ex)
                    //{
                    //    EngineBase.Log.Write("[PluginMain] PanelValues.Register Semiauto (error) " + ex.Message);
                    //}
                }
                else
                {
                    EngineBase.Log.Write("Native.AtsPanelValues.Register Semiauto - OutOfRange:" + Config.Schema.Vehicle.AtsPanelValues.Semiauto.ToString());
                }
                // - 全車・自車
                if (-2 < Config.Schema.Vehicle.AtsPanelValues.OneDoor && Config.Schema.Vehicle.AtsPanelValues.OneDoor < 1024)
                {
                    //try
                    //{
                    //    OneDoorPanelValue = Native.AtsPanelValues.RegisterBoolean(Config.Schema.Vehicle.AtsPanelValues.OneDoor, false, AtsEx.PluginHost.Binding.BindingMode.TwoWay);
                    //}
                    //catch (Exception ex)
                    //{
                    //    EngineBase.Log.Write("[PluginMain] PanelValues.Register OneDoor (error) " + ex.Message);
                    //}
                }
                else
                {
                    EngineBase.Log.Write("Native.AtsPanelValues.Register OneDoor - OutOfRange:" + Config.Schema.Vehicle.AtsPanelValues.OneDoor.ToString());
                }

                // サウンド設定
                // - ドアスイッチ入
                if (-2 < Config.Schema.Vehicle.AtsSounds.DoorSwitchOn && Config.Schema.Vehicle.AtsSounds.DoorSwitchOn < 1024)
                {
                    //try
                    //{
                    //    DoorSwitchOnSound = Native.AtsSounds.Register(Config.Schema.Vehicle.AtsSounds.DoorSwitchOn);
                    //}
                    //catch (Exception ex)
                    //{
                    //    EngineBase.Log.Write("[PluginMain] Sounds.Register DoorSwitchOn (error) " + ex.Message);
                    //}
                }
                else
                {
                    EngineBase.Log.Write("Native.AtsSounds.Register DoorSwitchOn - OutOfRange:" + Config.Schema.Vehicle.AtsSounds.DoorSwitchOn.ToString());
                }
                // - ドアスイッチ切
                if (-2 < Config.Schema.Vehicle.AtsSounds.DoorSwitchOff && Config.Schema.Vehicle.AtsSounds.DoorSwitchOff < 1024)
                {
                    //try
                    //{
                    //    DoorSwitchOffSound = Native.AtsSounds.Register(Config.Schema.Vehicle.AtsSounds.DoorSwitchOff);
                    //}
                    //catch (Exception ex)
                    //{
                    //    EngineBase.Log.Write("[PluginMain] Sounds.Register DoorSwitchOff (error) " + ex.Message);
                    //}
                }
                else
                {
                    EngineBase.Log.Write("Native.AtsSounds.Register DoorSwitchOff - OutOfRange:" + Config.Schema.Vehicle.AtsSounds.DoorSwitchOff.ToString());
                }
                // - 半自動ドア開
                if (-2 < Config.Schema.Vehicle.AtsSounds.DoorSemiautoOpen && Config.Schema.Vehicle.AtsSounds.DoorSemiautoOpen < 1024)
                {
                    //try
                    //{
                    //    DoorSemiautoOpenSound = Native.AtsSounds.Register(Config.Schema.Vehicle.AtsSounds.DoorSemiautoOpen);
                    //}
                    //catch (Exception ex)
                    //{
                    //    EngineBase.Log.Write("[PluginMain] Sounds.Register DoorSemiautoOpen (error) " + ex.Message);
                    //}
                }
                else
                {
                    EngineBase.Log.Write("Native.AtsSounds.Register DoorSemiautoOpen - OutOfRange:" + Config.Schema.Vehicle.AtsSounds.DoorSemiautoOpen.ToString());
                }
                // - 半自動ドア締
                if (-2 < Config.Schema.Vehicle.AtsSounds.DoorSemiautoClose && Config.Schema.Vehicle.AtsSounds.DoorSemiautoClose < 1024)
                {
                    //try
                    //{
                    //    DoorSemiautoCloseSound = Native.AtsSounds.Register(Config.Schema.Vehicle.AtsSounds.DoorSemiautoClose);
                    //}
                    //catch (Exception ex)
                    //{
                    //    EngineBase.Log.Write("[PluginMain] Sounds.Register DoorSemiautoClose (error) " + ex.Message);
                    //}
                }
                else
                {
                    EngineBase.Log.Write("Native.AtsSounds.Register DoorSemiautoClose - OutOfRange:" + Config.Schema.Vehicle.AtsSounds.DoorSemiautoClose.ToString());
                }

                // - デバッグ表示設定
                if (Config.Schema.ShowDebugLabel == true)
                {
                    ConfigAssistants.Assistant AS1 = this.Config.GetAssistant("HROneman", 40);
                    AssistantText = new AssistantText(new Mackoy.Bvets.AssistantSettings()
                    {
                        Name = AS1.Name,
                        Scale = AS1.Size,
                        AutoLocation = false,
                        Anchor = (AnchorStyles)AS1.Anchor,
                        Location = new System.Drawing.Point(AS1.X, AS1.Y)
                    });
                    BveHacker.MainForm.Assistants.Items.Add(AssistantText);
                }
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[PluginMain] (error) " + ex.Message);
            }
        }

        public override void Dispose()
        {
            BveHacker.ScenarioCreated -= OnScenarioCreated;
            BveHacker.MainFormSource.KeyDown -= OnKeyDown;
            if (!(AssistantText is null))
            {
                ConfigAssistants.Assistant AS1 = this.Config.GetAssistant("HROneman", 40);
                AS1.Anchor = (int)AssistantText.AssistantSettings.Anchor;
                AS1.X = AssistantText.AssistantSettings.Location.X;
                AS1.Y = AssistantText.AssistantSettings.Location.Y;
                BveHacker.MainForm.Assistants.Items.Remove(AssistantText);
            }
            this.Config.SaveAssistant();
        }

        private void OnScenarioCreated(ScenarioCreatedEventArgs e)
        {
            ConductorValve = new ConductorValve(e.Scenario.Vehicle.Instruments.Cab.Handles);
        }

        #region キー押し
        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            KeyPressed(new KeyInput(KeyKind_ENUM.キーボード, e.KeyValue));
        }
        private void KeyS_Press(object sender, EventArgs e)
        {
            try
            {
                KeyPressed(new KeyInput { Kind = KeyKind_ENUM.ATSキー, Code = 0 });
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[KeyS_Press] (error) " + ex.Message);
            }
        }
        private void KeyA1_Press(object sender, EventArgs e)
        {
            try
            {
                KeyPressed(new KeyInput { Kind = KeyKind_ENUM.ATSキー, Code = 1 });
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[KeyA1_Press] (error) " + ex.Message);
            }
        }
        private void KeyA2_Press(object sender, EventArgs e)
        {
            try
            {
                KeyPressed(new KeyInput { Kind = KeyKind_ENUM.ATSキー, Code = 2 });
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[KeyA2_Press] (error) " + ex.Message);
            }
        }
        private void KeyB1_Press(object sender, EventArgs e)
        {
            try
            {
                KeyPressed(new KeyInput { Kind = KeyKind_ENUM.ATSキー, Code = 3 });
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[KeyB1_Press] (error) " + ex.Message);
            }
        }
        private void KeyB2_Press(object sender, EventArgs e)
        {
            try
            {
                KeyPressed(new KeyInput { Kind = KeyKind_ENUM.ATSキー, Code = 4 });
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[KeyB2_Press] (error) " + ex.Message);
            }
        }
        private void KeyC1_Press(object sender, EventArgs e)
        {
            try
            {
                KeyPressed(new KeyInput { Kind = KeyKind_ENUM.ATSキー, Code = 5 });
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[KeyC1_Press] (error) " + ex.Message);
            }
        }
        private void KeyC2_Press(object sender, EventArgs e)
        {
            try
            {
                KeyPressed(new KeyInput { Kind = KeyKind_ENUM.ATSキー, Code = 6 });
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[KeyC2_Press] (error) " + ex.Message);
            }
        }
        private void KeyD_Press(object sender, EventArgs e)
        {
            try
            {
                KeyPressed(new KeyInput { Kind = KeyKind_ENUM.ATSキー, Code = 7 });
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[KeyD_Press] (error) " + ex.Message);
            }
        }
        private void KeyE_Press(object sender, EventArgs e)
        {
            try
            {
                KeyPressed(new KeyInput { Kind = KeyKind_ENUM.ATSキー, Code = 8 });
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[KeyE_Press] (error) " + ex.Message);
            }
        }
        private void KeyF_Press(object sender, EventArgs e)
        {
            try
            {
                KeyPressed(new KeyInput { Kind = KeyKind_ENUM.ATSキー, Code = 9 });
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[KeyF_Press] (error) " + ex.Message);
            }
        }
        private void KeyG_Press(object sender, EventArgs e)
        {
            try
            {
                KeyPressed(new KeyInput { Kind = KeyKind_ENUM.ATSキー, Code = 10 });
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[KeyG_Press] (error) " + ex.Message);
            }
        }
        private void KeyH_Press(object sender, EventArgs e)
        {
            try
            {
                KeyPressed(new KeyInput { Kind = KeyKind_ENUM.ATSキー, Code = 11 });
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[KeyH_Press] (error) " + ex.Message);
            }
        }
        private void KeyI_Press(object sender, EventArgs e)
        {
            try
            {
                KeyPressed(new KeyInput { Kind = KeyKind_ENUM.ATSキー, Code = 12 });
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[KeyI_Press] (error) " + ex.Message);
            }
        }
        private void KeyJ_Press(object sender, EventArgs e)
        {
            try
            {
                KeyPressed(new KeyInput { Kind = KeyKind_ENUM.ATSキー, Code = 13 });
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[KeyJ_Press] (error) " + ex.Message);
            }
        }
        private void KeyK_Press(object sender, EventArgs e)
        {
            try
            {
                KeyPressed(new KeyInput { Kind = KeyKind_ENUM.ATSキー, Code = 14 });
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[KeyK_Press] (error) " + ex.Message);
            }
        }
        private void KeyL_Press(object sender, EventArgs e)
        {
            try
            {
                KeyPressed(new KeyInput { Kind = KeyKind_ENUM.ATSキー, Code = 15 });
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[KeyL_Press] (error) " + ex.Message);
            }
        }
        private void KeyPressed(KeyInput keyInput)
        {
            try
            {
                //EngineBase.Log.Write("[KeyPressed] " + keyInput.Kind.ToString() + " " + keyInput.Code.ToString());

                KeyAssignSet keys = Config.Schema.Vehicle.KeyAssign;
                if (ConductorHost.IsEnabled)
                {

                    //if(this.NFB == true)
                    //{
                    if (keyInput.Equal(keys.LeftOpen) == true && !IsLeftOpenButtonPushed)
                    {
                        ConductorHost.Conductor.OpenDoors(DoorSide.Left);
                        SetSoundValue(Config.Schema.Vehicle.AtsSounds.DoorSwitchOn, 1, ref OldDoorSwitchOnSound);
                        //DoorSwitchOnSound?.Play();
                        IsLeftOpenButtonPushed = true;
                        EngineBase.Log.Write("[KeyPressed] LeftOpen");
                    }
                    else if (keyInput.Equal(keys.LeftClose) == true && !IsLeftCloseButtonPushed)
                    {
                        ConductorHost.Conductor.CloseDoors(DoorSide.Left);
                        SetSoundValue(Config.Schema.Vehicle.AtsSounds.DoorSwitchOn, 1, ref OldDoorSwitchOnSound);
                        //DoorSwitchOnSound?.Play();
                        IsLeftCloseButtonPushed = true;
                        EngineBase.Log.Write("[KeyPressed] LeftClose");
                    }
                    else if (keyInput.Equal(keys.LeftReopen) == true && !IsLeftReopenButtonPushed)
                    {
                        ConductorHost.Conductor.IsLeftReopening = true;
                        SetSoundValue(Config.Schema.Vehicle.AtsSounds.DoorSwitchOn, 1, ref OldDoorSwitchOnSound);
                        //DoorSwitchOnSound?.Play();
                        IsLeftReopenButtonPushed = true;
                        EngineBase.Log.Write("[KeyPressed] LeftReopen");
                    }
                    else if (keyInput.Equal(keys.RightOpen) == true && !IsRightOpenButtonPushed)
                    {
                        ConductorHost.Conductor.OpenDoors(DoorSide.Right);
                        SetSoundValue(Config.Schema.Vehicle.AtsSounds.DoorSwitchOn, 1, ref OldDoorSwitchOnSound);
                        //DoorSwitchOnSound?.Play();
                        IsRightOpenButtonPushed = true;
                        EngineBase.Log.Write("[KeyPressed] RightOpen");
                    }
                    else if (keyInput.Equal(keys.RightClose) == true && !IsRightCloseButtonPushed)
                    {
                        ConductorHost.Conductor.CloseDoors(DoorSide.Right);
                        SetSoundValue(Config.Schema.Vehicle.AtsSounds.DoorSwitchOn, 1, ref OldDoorSwitchOnSound);
                        //DoorSwitchOnSound?.Play();
                        IsRightCloseButtonPushed = true;
                        EngineBase.Log.Write("[KeyPressed] RightClose");
                    }
                    else if (keyInput.Equal(keys.RightReopen) == true && !IsRightReopenButtonPushed)
                    {
                        ConductorHost.Conductor.IsRightReopening = true;
                        SetSoundValue(Config.Schema.Vehicle.AtsSounds.DoorSwitchOn, 1, ref OldDoorSwitchOnSound);
                        //DoorSwitchOnSound?.Play();
                        IsRightReopenButtonPushed = true;
                        EngineBase.Log.Write("[KeyPressed] RightReopen");
                    }
                    //}

                    /*
                    if (e.KeyCode == Config.Schema.Vehicle.Keys.ConductorValve.Code)
                    {
                        ConductorValve.Pull();
                    }*/
                }

                //if (keyInput.Equal(keys.RequestFixStopPosition) == true)
                //{
                //    ConductorHost.Conductor.RequestFixStopPosition();
                //    EngineBase.Log.Write("[KeyPressed] RequestFixStopPosition :" + keyInput.Kind.ToString() + "," + keyInput.Code.ToString() + " = " + keys.RequestFixStopPosition.Kind.ToString() + ", " + keys.RequestFixStopPosition.Code.ToString());
                //}
                //else if (keyInput.Equal(keys.OnemanNFB) == true)
                //{
                //    this.NFB = !this.NFB;
                //    EngineBase.Log.Write("[KeyPressed] OnemanNFB");
                //}
                if (keyInput.Equal(keys.SemiautoSW) == true)
                {
                    ConductorHost.Conductor.PushSemiautoSW();
                    EngineBase.Log.Write("[KeyPressed] SemiautoSW");
                }
                else if (keyInput.Equal(keys.OneDoorSW) == true)
                {
                    ConductorHost.Conductor.PushOneDoorSW();
                    EngineBase.Log.Write("[KeyPressed] OneDoorSW");
                }
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[KeyPressed] (error) " + ex.Message);
            }
        }
        #endregion

        #region キー離し
        private void OnKeyUp(object sender, KeyEventArgs e)
        {
            KeyReleased(new KeyInput(KeyKind_ENUM.キーボード, e.KeyValue));
        }
        private void KeyS_Release(object sender, EventArgs e)
        {
            try
            {
                KeyReleased(new KeyInput { Kind = KeyKind_ENUM.ATSキー, Code = 0 });
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[KeyS_Release] (error) " + ex.Message);
            }
        }
        private void KeyA1_Release(object sender, EventArgs e)
        {
            try
            {
                KeyReleased(new KeyInput { Kind = KeyKind_ENUM.ATSキー, Code = 1 });
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[KeyA1_Release] (error) " + ex.Message);
            }
        }
        private void KeyA2_Release(object sender, EventArgs e)
        {
            try
            {
                KeyReleased(new KeyInput { Kind = KeyKind_ENUM.ATSキー, Code = 2 });
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[KeyA2_Release] (error) " + ex.Message);
            }
        }
        private void KeyB1_Release(object sender, EventArgs e)
        {
            try
            {
                KeyReleased(new KeyInput { Kind = KeyKind_ENUM.ATSキー, Code = 3 });
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[KeyB1_Release] (error) " + ex.Message);
            }
        }
        private void KeyB2_Release(object sender, EventArgs e)
        {
            try
            {
                KeyReleased(new KeyInput { Kind = KeyKind_ENUM.ATSキー, Code = 4 });
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[KeyB2_Release] (error) " + ex.Message);
            }
        }
        private void KeyC1_Release(object sender, EventArgs e)
        {
            try
            {
                KeyReleased(new KeyInput { Kind = KeyKind_ENUM.ATSキー, Code = 5 });
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[KeyC1_Release] (error) " + ex.Message);
            }
        }
        private void KeyC2_Release(object sender, EventArgs e)
        {
            try
            {
                KeyReleased(new KeyInput { Kind = KeyKind_ENUM.ATSキー, Code = 6 });
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[KeyC2_Release] (error) " + ex.Message);
            }
        }
        private void KeyD_Release(object sender, EventArgs e)
        {
            try
            {
                KeyReleased(new KeyInput { Kind = KeyKind_ENUM.ATSキー, Code = 7 });
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[KeyD_Release] (error) " + ex.Message);
            }
        }
        private void KeyE_Release(object sender, EventArgs e)
        {
            try
            {
                KeyReleased(new KeyInput { Kind = KeyKind_ENUM.ATSキー, Code = 8 });
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[KeyE_Release] (error) " + ex.Message);
            }
        }
        private void KeyF_Release(object sender, EventArgs e)
        {
            try
            {
                KeyReleased(new KeyInput { Kind = KeyKind_ENUM.ATSキー, Code = 9 });
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[KeyF_Release] (error) " + ex.Message);
            }
        }
        private void KeyG_Release(object sender, EventArgs e)
        {
            try
            {
                KeyReleased(new KeyInput { Kind = KeyKind_ENUM.ATSキー, Code = 10 });
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[KeyG_Release] (error) " + ex.Message);
            }
        }
        private void KeyH_Release(object sender, EventArgs e)
        {
            try
            {
                KeyReleased(new KeyInput { Kind = KeyKind_ENUM.ATSキー, Code = 11 });
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[KeyH_Release] (error) " + ex.Message);
            }
        }
        private void KeyI_Release(object sender, EventArgs e)
        {
            try
            {
                KeyReleased(new KeyInput { Kind = KeyKind_ENUM.ATSキー, Code = 12 });
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[KeyI_Release] (error) " + ex.Message);
            }
        }
        private void KeyJ_Release(object sender, EventArgs e)
        {
            try
            {
                KeyReleased(new KeyInput { Kind = KeyKind_ENUM.ATSキー, Code = 13 });
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[KeyJ_Release] (error) " + ex.Message);
            }
        }
        private void KeyK_Release(object sender, EventArgs e)
        {
            try
            {
                KeyReleased(new KeyInput { Kind = KeyKind_ENUM.ATSキー, Code = 14 });
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[KeyK_Release] (error) " + ex.Message);
            }
        }
        private void KeyL_Release(object sender, EventArgs e)
        {
            try
            {
                KeyReleased(new KeyInput { Kind = KeyKind_ENUM.ATSキー, Code = 15 });
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[KeyL_Release] (error) " + ex.Message);
            }
        }
        private void KeyReleased(KeyInput keyInput)
        {
            try
            {
                if (ConductorHost.IsEnabled)
                {
                    Data.KeyAssignSet keys = Config.Schema.Vehicle.KeyAssign;
                    //if(this.NFB == true)
                    //{
                    if (keyInput.Equal(keys.LeftOpen) == true && IsLeftOpenButtonPushed)
                    {
                        SetSoundValue(Config.Schema.Vehicle.AtsSounds.DoorSwitchOff, 1, ref OldDoorSwitchOffSound);
                        //DoorSwitchOffSound?.Play();
                        IsLeftOpenButtonPushed = false;
                    }
                    else if (keyInput.Equal(keys.LeftClose) == true && IsLeftCloseButtonPushed)
                    {
                        SetSoundValue(Config.Schema.Vehicle.AtsSounds.DoorSwitchOff, 1, ref OldDoorSwitchOffSound);
                        //DoorSwitchOffSound?.Play();
                        IsLeftCloseButtonPushed = false;
                    }
                    else if (keyInput.Equal(keys.LeftReopen) == true && IsLeftReopenButtonPushed)
                    {
                        ConductorHost.Conductor.IsLeftReopening = false;
                        SetSoundValue(Config.Schema.Vehicle.AtsSounds.DoorSwitchOff, 1, ref OldDoorSwitchOffSound);
                        //DoorSwitchOffSound?.Play();
                        IsLeftReopenButtonPushed = false;
                    }
                    else if (keyInput.Equal(keys.RightOpen) == true && IsRightOpenButtonPushed)
                    {
                        SetSoundValue(Config.Schema.Vehicle.AtsSounds.DoorSwitchOff, 1, ref OldDoorSwitchOffSound);
                        //DoorSwitchOffSound?.Play();
                        IsRightOpenButtonPushed = false;
                    }
                    else if (keyInput.Equal(keys.RightClose) == true && IsRightCloseButtonPushed)
                    {
                        SetSoundValue(Config.Schema.Vehicle.AtsSounds.DoorSwitchOff, 1, ref OldDoorSwitchOffSound);
                        //DoorSwitchOffSound?.Play();
                        IsRightCloseButtonPushed = false;
                    }
                    else if (keyInput.Equal(keys.RightReopen) == true && IsRightReopenButtonPushed)
                    {
                        ConductorHost.Conductor.IsRightReopening = false;
                        SetSoundValue(Config.Schema.Vehicle.AtsSounds.DoorSwitchOff, 1, ref OldDoorSwitchOffSound);
                        //DoorSwitchOffSound?.Play();
                        IsRightReopenButtonPushed = false;
                    }
                    //}
                }
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[KeyReleased] (error) " + ex.ToString());
            }
        }
        #endregion

        public override void Tick(TimeSpan elapsed)
        {
            try
            {
                ConductorHost.Tick();
                if (ConductorHost.IsEnabled)
                {
                    ConductorValve.Tick(BveHacker.Scenario.Vehicle.Conductor.Location.Speed);
                }

                // パネル状態
                //if (!(ModePanelValue is null)) ModePanelValue.Value = ConductorHost.Mode;
                SetPanelValue(Config.Schema.Vehicle.AtsPanelValues.Mode, (int)ConductorHost.Mode);
                //if (!(NFBPanelValue is null)) NFBPanelValue.Value = this.NFB;
                SetPanelValue(Config.Schema.Vehicle.AtsPanelValues.NFB, ConductorHost.IsEnabled);
                //if (!(NFBPanelValue is null))
                //{
                //    if(ConductorHost.IsEnabled) NFBPanelValue.Value = 1;
                //    else NFBPanelValue.Value = 0;
                //}
                SetPanelValue(Config.Schema.Vehicle.AtsPanelValues.Semiauto, ConductorHost.Conductor.SemiautoSW);
                //if (!(SemiAutoPanelValue is null)) SemiAutoPanelValue.Value = ConductorHost.Conductor.SemiautoSW;
                SetPanelValue(Config.Schema.Vehicle.AtsPanelValues.OneDoor, ConductorHost.Conductor.OneDoorSW);
                //if (!(OneDoorPanelValue is null)) OneDoorPanelValue.Value = ConductorHost.Conductor.OneDoorSW;

                // サウンド状態
                if (ConductorHost.Conductor.IsDoorClosed == false && ConductorHost.Conductor.OneDoorSW == false)
                {
                    // 自車のとき
                    SetSoundValue(Config.Schema.Vehicle.AtsSounds.DoorSemiautoOpen, 0, ref OldDoorSemiautoOpenSound); // PlayLoop
                }
                else
                {
                    SetSoundValue(Config.Schema.Vehicle.AtsSounds.DoorSemiautoOpen, -10000, ref OldDoorSemiautoOpenSound); // Stop
                }
                //if (!(DoorSemiautoOpenSound is null))
                //{
                //    if(ConductorHost.Conductor.IsDoorClosed == false && ConductorHost.Conductor.OneDoorSW == false)
                //    {
                //        // 自車のとき
                //        DoorSemiautoOpenSound.PlayLoop();
                //    }
                //    else
                //    {
                //        DoorSemiautoOpenSound.Stop();
                //    }
                //}
                if (ConductorHost.Conductor.IsDoorClosed == false)
                {
                    // 
                    SetSoundValue(Config.Schema.Vehicle.AtsSounds.DoorSemiautoClose, -10000, ref OldDoorSemiautoCloseSound); // Stop
                }
                else if (ConductorHost.Conductor.OneDoorSW == false)
                {
                    // 自車のとき
                    SetSoundValue(Config.Schema.Vehicle.AtsSounds.DoorSemiautoClose, 1, ref OldDoorSemiautoCloseSound); // Play
                }
                else
                {
                    SetSoundValue(Config.Schema.Vehicle.AtsSounds.DoorSemiautoClose, -10000, ref OldDoorSemiautoCloseSound); // Stop
                }
                //if (!(DoorSemiautoCloseSound is null))
                //{
                //    if (ConductorHost.Conductor.IsDoorClosed == false)
                //    {
                //        // 
                //        DoorSemiautoCloseSound.Stop();
                //    }
                //    else if (ConductorHost.Conductor.OneDoorSW == false)
                //    {
                //        // 自車のとき
                //        DoorSemiautoCloseSound.Play();
                //    }
                //    else
                //    {
                //        DoorSemiautoCloseSound.Stop();
                //    }
                //}


                if (!(AssistantText is null))
                {
                    var text = GetText();
                    AssistantText.Text = text.Text;
                    AssistantText.Color = text.Color;
                    AssistantText.BackgroundColor = text.BackgroundColor;
                }
            }
            catch (Exception ex)
            {
                EngineBase.Log.Write("[PluginMain.Tick] (error) " + ex.Message);
            }


            //return new VehiclePluginTickResult();


            (string Text, System.Drawing.Color Color, System.Drawing.Color BackgroundColor) GetText()
            {
                string tx = " (";
                //if (this.NFB == true)
                //{
                //    tx = " (ｵﾝ";
                //}
                //else
                //{
                //    tx = " (ｵﾌ";
                //}
                //tx += " ";
                if (ConductorHost.Conductor.SemiautoSW == false)
                {
                    tx += "自動";
                }
                else
                {
                    tx += "半自";
                }

                if (ConductorHost.Conductor.OneDoorSW == false)
                {
                    tx += " 自車";
                }
                else
                {
                    tx += " 全車";
                }

                if (ConductorHost.Conductor.IsDoorClosed == true)
                {
                    tx += " 締";
                }
                else
                {
                    tx += " 開";
                }
                tx += ")";

                switch (ConductorHost.Mode)
                {
                    case ConductorMode.Twoman:
                        return ("ツーマン", System.Drawing.Color.White, System.Drawing.Color.Blue);
                    case ConductorMode.TwomanAtNextStation:
                        return ("次駅からツーマン" + tx, System.Drawing.Color.Red, System.Drawing.Color.LightBlue);
                    case ConductorMode.Oneman:
                        return ("ワンマン" + tx, System.Drawing.Color.White, System.Drawing.Color.Green);
                    case ConductorMode.OnemanAtNextStation:
                        return ("次駅からワンマン" + tx, System.Drawing.Color.Red, System.Drawing.Color.LightGreen);
                    default:
                        throw new NotImplementedException();
                }
            }
        }

        private void SetPanelValue(int index, bool value)
        {
            if (value == true) 
            {
                SetPanelValue(index, 1);
            }
            else
            {
                SetPanelValue(index, 0);
            }
        }
        private void SetPanelValue(int index, int value)
        {
            if (-1 < index && index < 1024)
            {
                Native.AtsPanelArray[index] = value;
            }
        }

        private void SetSoundValue(int index, int value, ref int oldValue)
        {
            int nextValue = value;
            if (-1 < index && index < 1024)
            {
                switch (value)
                {
                    case -10000:// Const.AtsSoundControlInstruction.Stop:
                        break;
                    case 1:// Const.AtsSoundControlInstruction.Play:
                        if (oldValue == 1)
                        {
                            nextValue = 0; // 連続でPlayだと実質鳴らない
                        }
                        break;
                    case 0:// Const.AtsSoundControlInstruction.PlayLooping:
                        break;
                    case 2:// Const.AtsSoundControlInstruction.Continue:
                        break;
                    default:
                        break;
                }

                Native.AtsSoundArray[index] = nextValue;
                oldValue = value;
            }
        }

    }
}
